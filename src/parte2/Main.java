package parte2;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;

public class Main {
	//tama�o de los campos del registro binario
		static final int tam_codigo=4;
		static final int tam_descripcion=200;
		static final int tam_familia=16;
		static final int tam_precio=8;
		static final int tam_mValido=1;
		static final int tam_registro=tam_codigo+tam_descripcion+tam_familia+tam_precio+tam_mValido;
		static final int numproductos=7137;
		static final int tamproducto=229;
		public static void main(String[] args) {
			try {
			// Declaro RandomAccessFile para acceder a archivo
			RandomAccessFile raf= new RandomAccessFile("C:\\Users\\AlumnoManana20-21\\Downloads\\Productos.dat","rw");
			//declaramos variable en la que guardamos  el byte leido
			Byte valido;
			//declaramos int para guardar codigo
			int codigo;
			//declaro array de bytes para leer datos y string para leer
			byte [] bufferdescripcion= new byte[tam_descripcion];
			String descripcion;
			//declaro array de bytes para leer la familia y string para leer
			byte[] bufferfamilia= new byte[tam_familia];
			String familia;
			//declaro double en el que guardar precio
			double precio;
			//bucle que recorre el numero de productos existentes obteniendo numero de bytes del archivo y dividiendolo
			//entre numero de bytes de un producto
			for(int i=1;i<raf.length()/229;i++) {
				//posicionamos puntero en posicion dnde se encuentra marca de valido
				raf.seek((229*i)-1);
				
				valido=raf.readByte();
				if(valido==2) {
					System.out.println("Este es el "+i);
					//situa el puntero en la posicion inicial de la linea
					raf.seek(raf.getFilePointer()-229);
					
					codigo=raf.readInt();
					
					//guardo en string descripcion la descripcion
					raf.read(bufferdescripcion,0, tam_descripcion);
					descripcion= new String(bufferdescripcion,Charset.forName("UTF-8"));
					//guardo la familia en una String
					raf.read(bufferfamilia, 0, tam_familia);
					familia=new String(bufferfamilia, Charset.forName("UTF-8"));
					precio=raf.readDouble();
					
					System.out.print(codigo+" ");
					System.out.print(descripcion);
					System.out.print(familia+" ");
					System.out.print(precio+" ");
					System.out.println(valido+" ");
				}
				
			}
			raf.close();
			}catch(IOException e) {
				System.out.println(e);
			}
		}

	}


