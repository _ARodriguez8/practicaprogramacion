package parte1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class Practica {
	static Scanner sc=new Scanner(System.in);
	public static void main(String[] args) {
		try {
			//Creo 3 ArrayList para introducir los datos de los 3 ficheros
			ArrayList<String> nombreHombre=new ArrayList();
			ArrayList<String> nombreMujer=new ArrayList();
			ArrayList<String> apellidos=new ArrayList();
			int numPersonas;
			Persona p;
			
			//creo objeto properties
			Properties config=new Properties();
			config.load(new FileInputStream(new File("C:\\Users\\AlumnoManana20-21\\Desktop\\PracticaProg\\src\\Practica1\\Practica1.cfg")));
	
			//Creo 3 BufferedReader para leer datos e insertarlos en los ArrayList
			BufferedReader hombre=new BufferedReader(new FileReader(config.getProperty("fnombrehombre")));
			BufferedReader mujer=new BufferedReader(new FileReader(config.getProperty("fnombremujer")));
			BufferedReader apellido=new BufferedReader(new FileReader(config.getProperty("fapellidos")));
			
			//Creo BufferedWriter para escribir fichero personas
			BufferedWriter ficpersonas=new BufferedWriter(new FileWriter("C:\\Users\\AlumnoManana20-21\\Desktop\\PracticaProg\\src\\Practica1\\ficpersonas.txt"));
			
			//creo Bufferedwriter para escribir fichero Resultado
			BufferedWriter ficresultado=new BufferedWriter(new FileWriter("C:\\Users\\AlumnoManana20-21\\Desktop\\PracticaProg\\src\\Practica1\\ficresultado.txt"));
			
			//declaro array en el que introduzco los datos de hombres y mujeres
			int [] datosHombres=new int[2];
			int [] datosMujeres=new int[2];
			
			//declaro array en el que guardo a todas las mujeres y
			//y list en la que estan mujeres mas altas por orden alfabetico
			List<Persona> mujeres=new ArrayList<Persona>();
			List<Persona> mujeresAltas=new ArrayList<Persona>();
			
			//relleno arrays y cierro los BufferedReader
			nombreHombre=rellenaArray(hombre);
			hombre.close();
			nombreMujer=rellenaArray(mujer);
			mujer.close();
			apellidos=rellenaArray(apellido);
			apellido.close();
			numPersonas=Integer.parseInt(config.getProperty("numPersonas"));
			Persona[] personas=new Persona[numPersonas];
			
			for(int i=0;i<personas.length;i++) {
				p=generarPersona(i+1,nombreHombre, nombreMujer, apellidos);
				personas[i]=p;
			}
			for(int i=0;i<personas.length;i++) {
				ficpersonas.write(personas[i].obtenerDatos());
				ficpersonas.newLine();
			}
			ficpersonas.close();
			
			datosHombres=datosHombres(personas);
			datosMujeres=datosMujeres(personas);
			mujeres=obtenerMujeres(personas);
			mujeresAltas=mujeresOrdenadas(mujeres);
			
			//Escribimos en el fichero resultado los datos obtenidos
			ficresultado.write("Numero de hombres: "+datosHombres[0]);
			ficresultado.newLine();
			ficresultado.write("Porcentaje de hombres: "+datosHombres[1]);
			ficresultado.newLine();
			ficresultado.write("Numero de mujeres: "+datosMujeres[0]);
			ficresultado.newLine();
			ficresultado.write("Porcentaje de mujeres: "+datosMujeres[1]);
			ficresultado.newLine();
			ficresultado.write("10 mujeres mas jovenes ordenadas alfabeticamente");
			ficresultado.newLine();
			for(int i=0;i<mujeresAltas.size();i++) {
				ficresultado.write(mujeresAltas.get(i).getNombre());
				ficresultado.newLine();
			}
			ficresultado.close();
		}catch(IOException e){
			System.out.println(e);
		}
	}
	//Metodo que rellena ArrayList con contenido de fichero
	public static ArrayList<String> rellenaArray(BufferedReader x) throws IOException {
		String linea="";
		ArrayList<String> nombre=new ArrayList();
		while(linea!=null) {
			linea=x.readLine();
			if(linea!=null) {
				nombre.add(linea);
			}
		}
		x.close();
		return nombre;
	}
	public static Persona generarPersona(int i,ArrayList<String> nombreHombre, ArrayList<String> nombreMujer, ArrayList<String> apellidos) {
		//numSexo decidira el sexo de la persona
		double numSexo=Math.random();
		int nombre=(int)(Math.random()*(nombreMujer.size()));
		int apellido1=(int)(Math.random()*(apellidos.size()));
		int apellido2=(int)(Math.random()*(apellidos.size()));
		String esenombre= nombreMujer.get(nombre);
		String eseApellido1=apellidos.get(apellido1);
		String eseApellido2=apellidos.get(apellido2);
		String sexo;
		
	//	Persona p=new Persona();
		if(numSexo<=0.45) {
			nombre=(int)(Math.random()*(nombreHombre.size()));
			esenombre= nombreMujer.get(nombre);
			sexo="F";
		}else {
			nombre=(int)(Math.random()*(nombreHombre.size()));
			esenombre= nombreHombre.get(nombre);
			sexo="M";
		}
		Persona p=new Persona(esenombre,eseApellido1,eseApellido2,sexo,generarCodigo(i), generarFecha());//,LocalDate.now(),generarCodigo());
		return p;
	}
	public static LocalDate generarFecha() {
		LocalDate inicio= LocalDate.of(1910, 1, 1);
		int dias=(int) ChronoUnit.DAYS.between(inicio, LocalDate.now());
		long numero=(long)(Math.random()*dias+1);
		LocalDate fecha=inicio.plusDays(numero);
		return fecha;
	}
	public static String generarCodigo(int codigo) {
		String Codigo="C";
		String CodigoCompleto=Codigo+codigo;
		return CodigoCompleto;
	}
	public static int[] datosHombres(Persona [] personas) {
		int porcentajeH;
		int contadorH=0;
		int [] datos=new int[2];
		for(int i=0;i<personas.length;i++) {
			if(personas[i].sexo=="M") {
				contadorH++;
			}
		}
		porcentajeH=(contadorH*100)/personas.length;
		datos[0]=contadorH;
		datos[1]=porcentajeH;
		return datos;
	}
	public static int[] datosMujeres(Persona [] personas) {
		int [] datosHombres=new int[2];
		int[] datos=new int[2];
		int nMujeres;
		int porcentajeM;
		datosHombres=datosHombres(personas);
		nMujeres= personas.length-datosHombres[0];
		porcentajeM=(nMujeres*100)/personas.length;
		datos[0]=nMujeres;
		datos[1]=porcentajeM;
		return datos;
	}
	public static List<Persona> obtenerMujeres(Persona[] personas){
		List<Persona> mujeres=new ArrayList<Persona>();
		String p="";
		for(int i=0;i<personas.length;i++) {
			p=personas[i].getSexo();
			if(p=="F") {
				mujeres.add(personas[i]);
			}
		}
		return mujeres;
	}
	public static List<Persona> mujeresOrdenadas(List<Persona> mujeres){
		List<Persona> mujeres10=new ArrayList<Persona>(10);
		Persona p1=mujeres.get(0);
		Persona p2=mujeres.get(1);
		Comparator<Persona> porAltura=new Comparator<Persona>() {
			public int compare(Persona p1, Persona p2) {
				if(p1.getFnac().isBefore(p2.getFnac())) return 1;
				else return -1;
			}
		};
		Collections.sort(mujeres, porAltura);
		for(int i=0;i<10;i++) {
			mujeres10.add(mujeres.get(i));
		}
		Collections.sort(mujeres10, new Comparator<Persona>() {
			public int compare(Persona p1, Persona p2) {
				return p1.getNombre().compareTo(p2.getNombre());
			}
		});
		return mujeres10;
	}
}	


