package parte1;

import java.time.LocalDate;

public class Persona {
	public String nombre;
	public String apellido1;
	public String apellido2;
	public String sexo;
	public LocalDate fnac;
	public String codigo;
	
	public Persona(String nombre, String apellido1, String apellido2, String sexo, String codigo, LocalDate fnac) {
		this.nombre=nombre;
		this.apellido1=apellido1;
		this.apellido2=apellido2;
		this.sexo=sexo;
		this.fnac=fnac;
		this.codigo=codigo;
	}
	public Persona() {
		
	}
	@Override
	public String toString() {
		return "Nombre: "+nombre+" Primer Apellido: "+apellido1+" Segundo Apellido: "+apellido2+" Sexo: "+sexo+" Codigo: "+codigo+" Fecha de Nacimiento: "+fnac;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public LocalDate getFnac() {
		return fnac;
	}

	public void setFnac(LocalDate fnac) {
		this.fnac = fnac;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String obtenerDatos() {
		return codigo+";"+nombre+";"+apellido1+" "+apellido2+";"+fnac;
	}
}


